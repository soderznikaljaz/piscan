﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Collections.Specialized;

namespace ScanSANE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string username = ConfigurationManager.AppSettings.Get("username");
        private string password = ConfigurationManager.AppSettings.Get("password");
        private string hostname = ConfigurationManager.AppSettings.Get("hostname");
        private string path = ConfigurationManager.AppSettings.Get("path");
        private SshClient sshClient;
        SshCommand sc;
        DateTime dt = new DateTime();

        public MainWindow()
        {
            InitializeComponent();
            sshClient = new SshClient(hostname, username, password);
            try
            {
                sshClient.Connect();

                if (sshClient.IsConnected)
                {
                    setFilenameFromDateTime();
                    btnScan.IsEnabled = true;                 
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ni možno vzpostaviti povezave SSH. Preverite nastavitve.", "Napaka SSH", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
        }

        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            sc = sshClient.CreateCommand($"scanimage >{path}/{tbFilename.Text}.jpg --compression None --resolution {cbResolution.Text} --mode {mapColorType(cbColorType.Text)} --format jpeg ");
            btnScan.IsEnabled = false;
            Mouse.OverrideCursor = Cursors.Wait;

            Task.Run(() =>
            {               
                sc.Execute();               
            }
            
            ).ContinueWith(task =>
            {
                Mouse.OverrideCursor = Cursors.Arrow;
                btnScan.IsEnabled = true;
                MessageBox.Show("Skeniranje se je zaključilo. Dokument je viden v mapi Scans na omrežnem pogonu pishare.", "Skeniranje končano", MessageBoxButton.OK, MessageBoxImage.Information);
                setFilenameFromDateTime();
            }, TaskScheduler.FromCurrentSynchronizationContext());                     
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            sshClient.Disconnect();
            sshClient.Dispose();
        }

        private void setFilenameFromDateTime()
        {
            dt = DateTime.Now;
            tbFilename.Text = $"scan_{dt.Year}_{dt.Month}_{dt.Day}_{dt.Hour}_{dt.Minute}_{dt.Second}";
        }

        private string mapColorType(string selectedColorType)
        {
            switch (selectedColorType)
            {
                default:
                case "Barvna":
                    return "Color";
                case "Sivinska":
                    return "Gray";
                case "Lineart":
                    return "Lineart";
            } 
        }
    }
}
