# piScan

This tool allows me to use old USB printer connected to Raspberry Pi to scan documents from main PC via WiFi.

piScan is a Windows GUI tool that enables user to:
-  connect to Raspberry Pi via SSH (edit connection info in config file!),
-  use USB connected printer to scan document and save it on Raspberry Pi.

Note: SANE(Scanner Access Now Easy) must be installed on Raspberry Pi.
